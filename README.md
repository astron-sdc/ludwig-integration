# Ludwig Integration Test

This repo contains the integration test for Ludwig.
It is based on Selenium, which requires some additional packages to be available, therefore
it is recommended to run the integration test inside of docker.
It uses a docker bind volume to include the tests from the `tests` directory in the container.

## Running the integration test

```bash
export FE_VERSION=""    # container tag; defaults to latest
export BE_VERSION=""    # container tag; defaults to latest
docker compose run --rm integration pytest
```

## Work in progress

- Database migrations & data fixtures
- Test isolation (wrt. data in the DB)
