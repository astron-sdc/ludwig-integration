# Ludwig end-to-end tests

This repo contains the end-to-end tests for Ludwig. These tests attempt to simulate a 'real' testing situation as much as possible. Using `docker compose`, a version of Ludwig is launched that uses relevant services including a backend, database, workers etc. The tests are based on the popular end-to-end test framework [Cypress](https://www.cypress.io/). 

## Running the end-to-end tests 

To run the end-to-end tests fully in Docker just like the Gitlab runner would, you only need to run the script file `run_tests.sh`. This file will use `docker compose` to run the test suite. Test results will be displayed on the command line.

### Viewing screenshots of failed tests

Through the use of Docker volumes, screenshots of failed tests are persisted in the folder `cypress/screenshots`, where you can view them.

### Adding and modifying tests

Tests are located in the `cypress/e2e` folder (`*.cy.js`). You can add more tests or modify existing ones. Then you can simply rerun the tests suite, because the test files are also persisted through the use of Docker volumes.

Please look at the [Cypress documentation](https://docs.cypress.io/guides/core-concepts/introduction-to-cypress) for more info on locating elements.

### Attach a locally running Cypress to the e2e istance of Ludwig

For debugging purposes you may want to run the Cypress test runner locally, which interacts with the Ludwig e2e instance. From the `cypress` folder, simply run:

* `npm install`
* `npx cypress open`


### Run cypress e2e tests against a local backend and frontend

1. Set SRAM_ENABLED to 0 in your backend .env

2. Copy the fixtures from ludwig-integration to ludwig-backend/ludwig_api/fixtures 

    * ``` cp -r fixtures/ /path-to-ludwig-backend/ludwig_api/```

3. Run the commands to get a clean backend ready and load the data (run in ludwig-backend directory):

    * ```docker compose down --volumes```
    * ```make run```
    * ```make migrate```
    * ```docker compose exec backend python manage.py loaddata ludwig_api/fixtures/*```

4. Start the frontend `npm run dev`

5. Set `baseUrl` in `cypress.config.js` to the frontend url `http://localhost:5173`

6. Run `cd cypress; npx cypress open`

7. To clear the database after a test run:  `docker compose exec backend python manage.py flush --noinput`

## Linting

Install the Visual Code Extension ESLint to perform linting in VS Code. The `eslint-plugin-cypress` [recommended Cypress rules](https://github.com/cypress-io/eslint-plugin-cypress?tab=readme-ov-file#cypress-recommended) are enabled. If linting is not working, try running `npm install` from the cypress folder again.


## Work in progress

- Include a TMSS instance in the test setup
- Test authentication