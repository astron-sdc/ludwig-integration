const { defineConfig } = require("cypress");
const fs = require('fs');

module.exports = defineConfig({
  e2e: {
    viewportWidth: 1920,
    viewportHeight: 1080,
    fixturesFolder: "",
    setupNodeEvents(on, config) {
      // implement node event listeners here
      on('after:spec', (spec, results) => {
        // Remove videos for specs without failures or retries
        if (results && results.video) {
          const failures = results.tests.some((test) =>
            test.attempts.some((attempt) => attempt.state === 'failed')
          );
          if (!failures) {
            fs.unlinkSync(results.video);
          }
        }
      });
    },
        baseUrl: 'http://localhost:8085',
        experimentalMemoryManagement: true,
        retries: {
          openMode: 0,
          runMode: 2,
        }
  },
  video: true,
});
