// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

// Load userinfo api response from json fixture. Example esage:
// cy.interceptUserinfo("userinfo.json", "alias", () => { cy.visit('/foo/'); cy.wait("alias").then((i) => {...})})
Cypress.Commands.add("interceptUserinfo", (filename, alias, callback) => {
    cy.log(`Returning userinfo from file ${filename}`);
    cy.fixture(`fixtures/${filename}`).then((userinfo) => {
        cy.intercept(
            {
                method: 'GET',
                url: '/proposal-backend/api/v1/userinfo/',
            },
            userinfo
        ).as(alias);
        callback();
    });
});
