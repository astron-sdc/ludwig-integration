describe('Calls', () => {

    const createNewProposalButton = () => cy.contains("Call 1").parents("tr").contains("Create new proposal");
    const createCallButton = () => cy.contains("Create call for proposals");
    const titleInput = () => cy.contains("Title").find("input");
    const abstractInput = () => cy.contains("Abstract").find("textarea");
    const fileInput = () => cy.contains("Scientific Justification").parent().find("input");
    const createButton = () => cy.get("button").contains("Create");
    const justificationTab = () => cy.get("p").contains("Justification");
    const fileButton = () => cy.get("button").contains("LoremIpsum");

    it('are shown on the call overview page', () => {
        cy.visit('/proposal/calls/');
        cy.contains("Call 1");
        cy.contains("Call 3 (closed)");
    });

    it("it can create a proposal from the call page", () => {
        cy.visit("/proposal/calls");
        createNewProposalButton().click();
        titleInput().type("Proposal with uploaded file");
        abstractInput().type("Short and nice proposal");

        fileInput().selectFile("fixtures/LoremIpsumShort.pdf");

        createButton().click();
        cy.contains("Proposal was created successfully");
        // Allow some time for the proposal to be created
        cy.wait(5000);
        justificationTab().click();

        fileButton().should("have.text", "LoremIpsumShort.pdf (56.04 KiB)");
    });

    it("shows the call view page when View is clicked", () => {
        cy.visit("/proposal/calls");
        cy.contains("Call 3 (closed)").parents("tr").find('button:contains("View")').click();
        cy.location('pathname').should('eq', '/proposal/calls/3/view');
    });

    it("creation button is not shown if not logged in as admnistrator", () => {
        cy.interceptUserinfo("userinfo.json", "userinfo", () => {
            cy.visit('/proposal/calls');
            cy.wait('@userinfo').then((interception) => {
                createCallButton().should('not.exist');
                cy.contains("Call 3 (closed)");
            }
            );
        });
    });

    it("creation button is shown if logged in as admnistrator", () => {
        cy.interceptUserinfo("userinfoadmin.json", "userinfoadmin", () => {
            cy.visit('/proposal/calls');
            cy.wait('@userinfoadmin').then((interception) => {
                createCallButton().should('exist');
                cy.contains("Call 3 (closed)");
            }
            );
        });
    });
});
