describe("Reviews", () => {
    const assignReviewersDropdownButton = () => cy.contains("Assign reviewers");
    const assignPrimaryReviewerButton = () => cy.contains("Primary reviewer");
    const modalConfirmAssignButton = () => cy.get("footer").contains("Assign");

    const member = {
        name: "Dev Op",
        eduperson_unique_id: "123123123",
        email: "dev@op",
        scoped_affiliation: "dev@op"
    };

    const callGroups = [
        { name: "Techinical Reviewer", short_name: "tech_reviewer", members: [member] },
        { name: "Scientific Reviewer", short_name: "sci_reviewer", members: [member] }
    ];

    const callMembers = {
        groups: callGroups,
        collaboration_url: "",
        members_url: "",
        groups_url: "",
        invite_link: "",
        join_requests_url: "",
        members: [member]
    };

    const proposalUnderReview = [
        {
            id: 6,
            submitted_at: "2021-01-01T00:00:00Z",
            created_at: "2021-01-01T00:00:00Z",
            title: "Proposal under review (to be assigned)",
            status: "under_review",
            project_code: "CALL3_0000",
            groups: [
                {
                    name: "PI",
                    description: "some group description",
                    short_name: "pi",
                    members: callMembers.members
                }
            ],
            reviews: []
        }
    ];

    describe('that are assignable', () => {
        it("are shown in the review assignment table for the call overview page", () => {
            cy.intercept({
                method: "GET",
                url: '/proposal-backend/api/v1/calls/3/proposals/'
            }, proposalUnderReview).as('getProposalsUnderReview');
            cy.visit("/proposal/calls/3/view");
            cy.wait('@getProposalsUnderReview').then((interception) => {
                cy.get('.animate-spinner').should('not.exist');
                assignReviewersDropdownButton();
                cy.get('td:contains("Assign reviewer")').should('have.length', 4);
            });
        });

        it("can be assigned to a call member", () => {
            cy.intercept({
                method: "GET",
                url: '/proposal-backend/api/v1/calls/3/proposals/'
            }, proposalUnderReview).as('getProposalsUnderReview');
            cy.intercept({
                method: "GET",
                url: '/proposal-backend/api/v1/calls/3/members/'
            }, callMembers).as('getCallMembers');
            cy.visit("/proposal/calls/3/view");
            cy.wait('@getProposalsUnderReview').then((interception) => {
                cy.wait('@getCallMembers').then((callMemberInterception) => {
                    cy.contains("Technical").parent().find("input[type='checkbox']").click({ force: true });
                    assignReviewersDropdownButton().click();
                    assignPrimaryReviewerButton().click();
                    cy.contains("Proposal title: Proposal under review (to be assigned)");
                    cy.contains("Select Primary Technical Reviewer").parent("div").find("input[type='text']").click();
                    cy.get("span").contains(callMembers.members[0].name).as("callMemberToAssign");
                    cy.get("@callMemberToAssign").click();
                    modalConfirmAssignButton().click();
                    cy.contains("Reviewer was successfully assigned.");
                });
            });
        });

        it("cannot be assigned if no prosals are selected", () => {
            cy.intercept({
                method: "GET",
                url: '/proposal-backend/api/v1/calls/3/proposals/'
            }, proposalUnderReview).as('getProposalsUnderReview');
            cy.visit("/proposal/calls/3/view");
            cy.wait('@getProposalsUnderReview').then((interception) => {
                assignReviewersDropdownButton().should("be.disabled");
            });
        });

        it("cannot be assigned if there are no call members", () => {
            cy.intercept({
                method: "GET",
                url: '/proposal-backend/api/v1/calls/3/proposals/'
            }, proposalUnderReview).as('getProposalsUnderReview');
            cy.visit("/proposal/calls/3/view");
            cy.wait('@getProposalsUnderReview').then((interception) => {
                cy.contains("Technical").parent().find("input[type='checkbox']").click({ force: true });
                assignReviewersDropdownButton().click();
                assignPrimaryReviewerButton().click();
                cy.contains("This call has no technical panel members.");
            });
        });

        it("shows two dropdowns if two panel types are selected", () => {
            cy.intercept({
                method: "GET",
                url: '/proposal-backend/api/v1/calls/3/proposals/'
            }, proposalUnderReview).as('getProposalsUnderReview');
            cy.visit("/proposal/calls/3/view");
            cy.wait('@getProposalsUnderReview').then((interception) => {
                cy.contains("Technical").parent().find("input[type='checkbox']").click({ force: true });
                cy.contains("Scientific").parent().find("input[type='checkbox']").click({ force: true });
                assignReviewersDropdownButton().click();
                assignPrimaryReviewerButton().click();
                cy.contains("This call has no technical panel members.");
                cy.contains("This call has no scientific panel members.");
                modalConfirmAssignButton().should("be.disabled");
            });
        });
    });
});
