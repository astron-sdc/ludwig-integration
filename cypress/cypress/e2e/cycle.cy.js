describe('Cycles', () => {
    it('are shown on the cycle overview page', () => {
        cy.visit('/proposal/cycles/');
        // TODO: assert presence of cycles
    });

    // TODO: finish this test
    it("can be created", () => {
        cy.intercept(
            {
                method: 'GET',
                url: '/proposal-backend/api/v1/tmss/cycles/',
            },
            [
                {
                    name: "Cycle 1",
                    url: "disney.com",
                    created_at: "2021-09-01T00:00:00Z",
                    updated_at: "2021-09-01T00:00:00Z",
                    description: "Test cycle",
                    duration: 0,
                    project: [],
                    project_ids: [],
                    start: "2021-09-01T00:00:00Z",
                    stop: "2021-09-01T00:00:00Z",
                    tags: "2021-09-01T00:00:00Z",
                    quota: [],
                    qupta_ids: [],

                },
            ]
        ).as('getCycles');

        cy.visit('/proposal/cycles/');

        cy.contains("Create Cycle").click();

        cy.wait('@getCycles').then((interception) => {
            cy.contains("Name").find("input").type("Test Cycle");
            cy.contains("Code").find("input").type("C123", { force: true }); // TODO: mock call to TMSS
            cy.contains("Description").find("textarea").type("A test cycle");
            //cy.contains("Save").click();
        });
    });
});