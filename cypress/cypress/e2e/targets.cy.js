describe('Targets', () => {
    const targetsTab = () => cy.contains("Target objects");
    const addTargetButton = () => cy.contains("Add Target"); 
    const saveTargetsButton = () => cy.contains("Save targets");

    const firstRowResolveButton = () => cy.contains("Resolve"); 
    const firstRowTableBody = () => firstRowResolveButton().parentsUntil("tbody");

    const firstRowNameInput = () => firstRowTableBody().find("input").eq(0);
    const firstRowRaInput = () => firstRowTableBody().find("input").eq(1);
    const firstRowDecInput = () => firstRowTableBody().find("input").eq(2);

    it("can be saved", () => {
        cy.visit("/proposal/proposals/7/");

        targetsTab().click();
        cy.wait(500);
        addTargetButton().click();
        cy.wait(500);
        firstRowNameInput().type("m71");
        firstRowResolveButton().click();

        cy.wait(3000);

        firstRowRaInput().should("have.value", "19:53:46.49");
        firstRowDecInput().should("have.value", "+18:46:45.1");

        saveTargetsButton().click();

        cy.contains("Targets were saved successfully");
    });
});