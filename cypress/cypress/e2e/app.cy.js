describe('LUDWIG', () => {
  it('is up and running', () => {
    cy.visit('/proposal/');
    cy.title().should('eq', 'ASTRON | TULP Proposal Tool');
  });

  describe("The sidebar", () => {
    const callSidebarItem = () => cy.get('[href="/proposal/calls/"]');
    const cycleSidebarItem = () => cy.get('[href="/proposal/cycles/"]');
    const createCycleButton = () => cy.contains("Create Cycle");

    it('should not show Cycles in sidebar if not administrator', () => {
      cy.interceptUserinfo("userinfo.json", "userinfo", () => {
        cy.visit('/proposal/');
        cy.wait('@userinfo').then((interception) => {
          cy.title().should('eq', 'ASTRON | TULP Proposal Tool');
          callSidebarItem().should('exist');
          cycleSidebarItem().should('not.exist');
        }
        );
      });
    });

    it('should show Cycles in sidebar if administrator and redirect to Cycle creation page when clicked', () => {
      cy.interceptUserinfo("userinfoadmin.json", "userinfoadmin", () => {
        cy.visit('/proposal/');
        cy.wait('@userinfoadmin').then((interception) => {
          cy.title().should('eq', 'ASTRON | TULP Proposal Tool');
          cy.contains("Dev Op (Admin)");
          callSidebarItem().should('exist');
          cycleSidebarItem().click();
          createCycleButton().click();
          cy.url().should('include', 'proposal/cycles/create/');
        }
        );
      });
    });
  });
});