describe('Proposals', () => {

    describe('on the proposal create page', () => {
        it('can be created', () => {
            cy.visit('/proposal/proposals/create/');
            cy.contains("Title").find("input").type("Baby's first proposal");
            cy.contains("Abstract").find("textarea").type(
                "I get knocked down, but I get up again\nYou are never gonna keep me down\n" +
                "I get knocked down, but I get up again\nYou are never gonna keep me down\n");
        });
    });

    describe("on the proposal edit page", () => {

        const proposalStatusInput = () => cy.contains("Status").parent().find("blockquote");
        const acceptProposalButton = () => cy.contains("Accept Proposal");
        const rejectProposalButton = () => cy.contains("Reject Proposal");  
        const submitProposalButton = () => cy.contains("Submit proposal");
        const ratingInput = () => cy.contains("rating").find("input");
        const gradeButton = () => cy.contains("Grade proposal");

        it("can be accepted", () => {
            cy.visit("/proposal/proposals/1/");

            proposalStatusInput().should("have.text", "under review");
    
            acceptProposalButton().click();
    
            proposalStatusInput().should("have.text", "accepted");
        });
    
        it("can be rejected", () => {
            cy.visit("/proposal/proposals/2/");
                   
            proposalStatusInput().should("have.text", "under review");
    
            rejectProposalButton().click();
    
            proposalStatusInput().should("have.text", "rejected");
        });

        it("can be submitted (without direct action)", () => {
            cy.visit("/proposal/proposals/3/");

            proposalStatusInput().should("have.text", "draft");
    
            submitProposalButton().click();
    
            proposalStatusInput().should("have.text", "submitted");
        });

        it("can be submitted (with direct action)", () => {
            cy.visit("/proposal/proposals/4/");

            proposalStatusInput().should("have.text", "draft");
    
            submitProposalButton().click();
    
            proposalStatusInput().should("have.text", "under review");
        });

        it("can be rated", () => {
            cy.visit("/proposal/proposals/5/");

            ratingInput().type("5");

            gradeButton().click();

            cy.visit("/proposal/proposals/5/");

            ratingInput().should("have.value", "5");
        });
    });
});
