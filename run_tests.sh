#!/bin/sh
set -e

docker compose pull

echo "Using the following docker images:"
docker images --digests registry.gitlab.com/astron-sdc/ludwig-*

docker compose up -d frontend
sleep 5
docker compose exec backend python manage.py migrate
docker compose exec backend python manage.py createcachetable

docker compose exec backend python manage.py loaddata \
    ludwig_api/fixtures/cycles \
    ludwig_api/fixtures/calls \
    ludwig_api/fixtures/proposals

docker compose run --rm e2e
