FROM cypress/browsers:latest

WORKDIR /cypress

COPY cypress .

ENV CI=1

RUN npm ci

RUN npx cypress verify
