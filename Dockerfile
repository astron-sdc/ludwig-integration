# Installs Selenium dependencies

# Webdrivermanager does not support 3.12 yet
FROM python:3.11

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y firefox-esr build-essential tree curl

WORKDIR /workdir

COPY requirements.txt /workdir/requirements.txt
RUN pip install -r requirements.txt

RUN webdrivermanager firefox:v0.34.0 --linkpath /usr/local/bin

WORKDIR /workdir
CMD ["/bin/bash"]
